﻿using ExchangeRatesProject.BusinessApi.Abstract;
using ExchangeRatesProject.DataTransfer;
using ExchangeRatesProjectUnitOfWork.Abstract;
using ExchangeRatesProjectUnitOfWork.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRatesProject.BusinessApi.Concrete
{
    public class ExchangeRateBusinessApi : IExchangeRateBusinessApi
    {
        private IBaseUoW _uow;
        private IBaseUoW uow
        {
            get
            {
                if (_uow == null) _uow = new BaseUow();
                return _uow;
            }
        }
        public List<ExchangeRateDt> GetAll()
        {
            List<ExchangeRateDt> exchangeRates = uow.ExchangeRateApi.Select().Select(x => new ExchangeRateDt()
            {
                UsdA = x.UsdA,
                UsdS = x.UsdS,
                EurA = x.EurA,
                EurS = x.EurS,
                Date = x.Date
            }).ToList();
            return exchangeRates;
        }
    }
}
