﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ExchangeRatesProject.DataTransfer
{
    public  class ExchangeRateDt
    {
        public string UsdA { get; set; }
        public string UsdS { get; set; }
        public string EurA { get; set; }
        public string EurS { get; set; }
        public string Date { get; set; }
    }
}
