﻿using ExchangeRatesProject.Entity;
using ExchangeRatesProject.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRatesProject.DatabaseApi.Abstract
{
    public interface IExchangeRateApi :IBaseRepository<ExchangeRate>
    {
    }
}
