﻿using ExchangeRatesProject.DatabaseApi.Abstract;
using ExchangeRatesProject.Entity;
using ExchangeRatesProject.Repository.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRatesProject.DatabaseApi.Concrete
{
    public class ExchangeRateApi : BaseRepository<ExchangeRate>,IExchangeRateApi
    {
        public ExchangeRateApi(DbContext dbContext)
            :base(dbContext)
        {

        }
    }
}
