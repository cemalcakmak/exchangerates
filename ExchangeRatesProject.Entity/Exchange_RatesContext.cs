﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ExchangeRatesProject.Entity
{
    public partial class Exchange_RatesContext : DbContext
    {
        public Exchange_RatesContext()
        {
        }

        public Exchange_RatesContext(DbContextOptions<Exchange_RatesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ExchangeRate> ExchangeRates { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("host=127.0.0.1;port=5432;database=Exchange_Rates;user id=postgres;password=159753");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Turkish_Turkey.1254");

            modelBuilder.Entity<ExchangeRate>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Date).HasColumnType("character varying");

                entity.Property(e => e.EurA)
                    .HasMaxLength(10)
                    .HasColumnName("EUR_A");

                entity.Property(e => e.EurS)
                    .HasMaxLength(10)
                    .HasColumnName("EUR_S");

                entity.Property(e => e.UsdA)
                    .HasMaxLength(10)
                    .HasColumnName("USD_A");

                entity.Property(e => e.UsdS)
                    .HasMaxLength(10)
                    .HasColumnName("USD_S");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
