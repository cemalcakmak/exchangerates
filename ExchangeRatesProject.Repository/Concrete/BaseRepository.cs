﻿using ExchangeRatesProject.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRatesProject.Repository.Concrete
{
    public class BaseRepository<T> : IBaseRepository<T>
        where T : class, new()
    {
        protected readonly DbContext dbContext;
        public BaseRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public IQueryable<T> Select(Expression<Func<T, bool>> predicate = null)
        {
            IQueryable<T> quaryable = dbContext.Set<T>().AsQueryable();
            if (predicate != null) quaryable = quaryable.Where(predicate);
            return quaryable;
        }
    }
}
