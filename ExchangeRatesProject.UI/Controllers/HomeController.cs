﻿using ExchangeRatesProject.UI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRatesProject.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Graphics(string excr)
        {
            List<ExchangeModel> exclist = new List<ExchangeModel>();
            RestAPISend RAS = new RestAPISend();
            var asd = RAS.ERPGet();
            foreach (var item in asd)
            {
                if (excr == "USDA")
                {
                    exclist.Add(new ExchangeModel { Date = item.date, Unit = item.usdA });
                }else if (excr == "USDS")
                {
                    exclist.Add(new ExchangeModel { Date = item.date, Unit = item.usdS });
                }
                else if (excr == "EURA")
                {
                    exclist.Add(new ExchangeModel { Date = item.date, Unit = item.eurA });
                }
                else if (excr == "EURS")
                {
                    exclist.Add(new ExchangeModel { Date = item.date, Unit = item.eurS });
                }
                else
                {
                    exclist = null;
                }
            }

            return Json(exclist);
        }


    }
}
