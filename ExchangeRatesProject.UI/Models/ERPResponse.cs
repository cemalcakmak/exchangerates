﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRatesProject.UI.Models
{
    public class ERPResponse
    {
        public string usdA { get; set; }
        public string usdS { get; set; }
        public string eurA { get; set; }
        public string eurS { get; set; }
        public string date { get; set; }

    }
}
