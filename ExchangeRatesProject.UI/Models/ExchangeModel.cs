﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRatesProject.UI.Models
{
    public class ExchangeModel
    {
        public string Unit { get; set; }
        public string Date { get; set; }
    }
}
