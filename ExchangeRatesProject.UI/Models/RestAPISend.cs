﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRatesProject.UI.Models
{
    public class RestAPISend
    {
        public List<ERPResponse> ERPGet()
        {
            var client = new RestClient("https://localhost:44394/api/ExchangeRate");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "992e333a-dd66-d59b-3ada-3ea54c1ff24e");
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);

            List<ERPResponse> ERPL = new List<ERPResponse>();
            string Contentrestest = response.Content;
            ERPL = JsonConvert.DeserializeObject<List<ERPResponse>>(Contentrestest);

            return ERPL;
        }
    }
}
