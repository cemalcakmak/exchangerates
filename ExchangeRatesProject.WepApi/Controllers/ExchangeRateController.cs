﻿using ExchangeRatesProject.BusinessApi.Abstract;
using ExchangeRatesProject.BusinessApi.Concrete;
using ExchangeRatesProject.DataTransfer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRatesProject.WepApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeRateController : ControllerBase
    {
        private readonly IExchangeRateBusinessApi exchangeRateBusinessApi;
        public ExchangeRateController(IExchangeRateBusinessApi exchangeRateBusinessApi)
        {
            this.exchangeRateBusinessApi = exchangeRateBusinessApi;
                     
        }

        [HttpGet]

        public IEnumerable<ExchangeRateDt> Get()
        {
            return exchangeRateBusinessApi.GetAll();
        }
    }
}
