\connect Exchange_Rates

CREATE TABLE IF NOT EXISTS public."ExchangeRate"
(
    "USD_A" character varying(10) COLLATE pg_catalog."default",
    "USD_S" character varying(10) COLLATE pg_catalog."default",
    "EUR_A" character varying(10) COLLATE pg_catalog."default",
    "EUR_S" character varying(10) COLLATE pg_catalog."default",
    "Date" character varying COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."ExchangeRate"
    OWNER to postgres;