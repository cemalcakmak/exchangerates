﻿using ExchangeRatesProject.DatabaseApi.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRatesProjectUnitOfWork.Abstract
{
    public interface IBaseUoW : IDisposable
    {
        IExchangeRateApi ExchangeRateApi { get;  }
    }
}
