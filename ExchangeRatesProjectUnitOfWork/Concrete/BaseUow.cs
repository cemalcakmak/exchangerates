﻿using ExchangeRatesProject.DatabaseApi.Abstract;
using ExchangeRatesProject.DatabaseApi.Concrete;
using ExchangeRatesProject.Entity;
using ExchangeRatesProjectUnitOfWork.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRatesProjectUnitOfWork.Concrete
{
    public class BaseUow : IBaseUoW
    {
        private static DbContext _dbContext;
        private static DbContext dbContext
        {
            get
            {
                if (_dbContext == null) _dbContext = new Exchange_RatesContext();
                return _dbContext;
            }

        }
        public IExchangeRateApi ExchangeRateApi => new ExchangeRateApi(dbContext);

        public void Dispose()
        {
            GC.SuppressFinalize(dbContext);
        }
    }
}
